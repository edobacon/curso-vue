import Vue from "vue";
import App from "./App.vue";
import GlobalComp from "./components/GlobalComp.vue";

Vue.config.productionTip = false;

Vue.component("global-comp", GlobalComp);

new Vue({
  render: (h) => h(App),
}).$mount("#app");
