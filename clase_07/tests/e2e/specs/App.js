describe('App Test', () => { 
  
  it('Load the main container', () => {
    cy.visit('http://localhost:8080/');
    cy.get('#app').should('be.visible');
  });

  it('Display prop title on h1', () => {
    cy.get('h1').should('contain', '¡ Super Todo List !');
  });

  it('Display error text on try with empty description', () => {
    cy.get('.btn').click();
    cy.get('.error-text').should('be.visible');
  });

  it('todo list should start empty', () => {
    cy.get('.todos-pending-none').should('be.visible');
  });

  it('Add todo to todo list', () => {
    cy.get('input#description').type('Test 1');
    cy.get('.btn').click();
    cy.get('.btn').should('contain', '...Añadiendo');
    cy.get('.todo-item p').first().should('contain', 'Test 1');
  });

  it('Error message should be hidden after success', () => {
    cy.get('.error-text').should('not.be.visible');
  });

  it('Complete todo btn should be visible by default', () => {
    cy.get('.complete-btn').should('be.visible');
    cy.get('.pending-btn').should('not.be.visible');
  });

  it('complete Todo', () => {
    cy.get('.todo-item .complete-btn').first().click();
    cy.get('.todo-item p').first().should('have.class', 'line-through');
  })

  it('re enable todo', () => {
    cy.get('.pending-btn').should('be.visible');
    cy.get('.todo-item .pending-btn').first().click();
    cy.get('.todo-item p').first().should('not.have.class', 'line-through');
  });

});
