import { mount, createLocalVue } from '@vue/test-utils';
import App from '@/App';
import TodoList from '@/components/TodoList';
import TodoForm from '@/components/TodoList';
import Vuex from 'vuex';

export const store = {
  state: {
    TODOS: [],
    IS_ADDING_TODO: false
  },
  actions: {
    ADD_TODO: jest.fn()
  },
  getters: {
    TODOS: () => [],
    IS_ADDING_TODO: () => false
  }
};

let wrapper;
const localVue = createLocalVue();
localVue.use(Vuex);
const localStore = new Vuex.Store(store);

beforeEach(() => {
  wrapper = mount(App, {
    localVue,
    store: localStore
  });
});

describe('App', () => {
  it('mounts correctly', () => {
    
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('Load Todo List component', () => {
    expect(wrapper.findComponent(TodoList).exists()).toBe(true);
  });

  it('Load Todo Form component', () => {
    expect(wrapper.findComponent(TodoForm).exists()).toBe(true);
  })
});
