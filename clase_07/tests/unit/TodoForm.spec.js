import { createLocalVue, mount } from '@vue/test-utils';
import TodoForm from '@/components/TodoForm';
import { store } from './App.spec';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);
const localStore = new Vuex.Store(store);

let wrapper;
const onSubmit = jest.spyOn(TodoForm.methods, 'onSubmit');
const isMountedFn = jest.spyOn(TodoForm.methods, 'isMountedFn');
const ADD_TODO = jest.spyOn(TodoForm.methods, 'ADD_TODO');
const title = 'Test Title';

store.dispatch = jest.fn();

beforeEach(() => {
  wrapper = mount(TodoForm, {
    localVue,
    store: localStore,
    propsData: {
      title
    }
  });
});

describe('Todo Form mount', () => {

  it('mounts correctly', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('renders props data', () => {
    expect(wrapper.find('h1').text()).toBe(title);
  });

  it('render btn on wait status', () => {
    expect(wrapper.find('.btn').text()).toBe('Crear Tarea');
  });

  it('hidden error text', () => {
    expect(wrapper.find('.error-text').classes()).toContain('hidden');
  });

  it('calls isMounted on mount', () => {
    expect(isMountedFn).toBeCalled();
    expect(wrapper.vm.isMounted).toEqual(true);
  });

  it('renders correct btn label', () => {
    expect(wrapper.vm.btnText).toEqual('Crear Tarea');
  });

});

describe('Todo Form vuex', () => {

  it('show error on empty description', async () => {
    wrapper.find('form').trigger('submit.prevent');
    expect(onSubmit).toBeCalled();
    await localVue.nextTick();
    expect(wrapper.find('.error-text').classes()).not.toContain('hidden');
  });

  it('set input value to property', () => {
    const value = 'some value';
    wrapper.find('input').setValue(value);
    expect(wrapper.vm.description).toEqual(value);
  });

  it('dispatch action when input is filled', async () => {
    const description = 'value';
    wrapper.find('input').setValue(description);
    wrapper.vm.onSubmit();
    expect(ADD_TODO).toBeCalledWith(expect.objectContaining({ description }));
  });

  it('show error on empty description', () => {
    wrapper.find('input').setValue('');
    expect(wrapper.find('.error-text').classes()).toContain('hidden');
  });

});
