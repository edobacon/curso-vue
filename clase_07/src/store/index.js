import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const delay = () => new Promise(resolve => setTimeout(resolve, 1000));

export const store = {
  state: {
    TODOS: [],
    IS_ADDING_TODO: false
  },
  mutations: {
    SET_ADDING_STATUS(state, status) {
      state.IS_ADDING_TODO = status;
    },
    ADD_TODO(state, todo) {
      state.TODOS.push(todo);
    },
    CHANGE_TODO_STATUS(state, id) {
      const todo = state.TODOS.find(el => el.id === id);
      todo && (todo.isComplete = !todo.isComplete);
    }
  },
  actions: {
    async ADD_TODO({ state, commit }, { id,  description}) {
      if (state.IS_ADDING_TODO) return;
      try {
        commit('SET_ADDING_STATUS', true);
        await delay();
        commit('ADD_TODO', { id, description, isComplete: false });
      } catch (error) {
        console.log(error);
      } finally {
        commit('SET_ADDING_STATUS', false);
      }
    },
    async CHANGE_TODO_STATUS({ state, commit }, id) {
      if (state.IS_ADDING_TODO) return;
      try {
        await delay();
        commit('CHANGE_TODO_STATUS', id);
      } catch (error) {
        console.log(error);
      }
    }
  },
  getters: {
    TODOS: state => state.TODOS,
    IS_ADDING_TODO: state => state.IS_ADDING_TODO
  },
}

export default new Vuex.Store(store);
