import Vue from "vue";
import VueRouter from "vue-router";
import View1 from "../views/View1.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/view-1",
  },
  {
    path: "/view-1",
    name: "view.1",
    component: View1,
  },
  {
    path: "/view-2",
    name: "view.2",
    component: () =>
      import(/* webpackChunkName: "view-2" */ "../views/View2.vue"),
  },
  {
    path: "/view-3/",
    component: () => import("../layout/Layout.vue"),
    children: [
      {
        path: "index",
        name: "view.3.index",
        component: () =>
          import(/* webpackChunkName: "view-3" */ "../views/View3.vue"),
        meta: {
          private: true,
        },
      },
      {
        path: "sib",
        name: "view.3.sibling",
        component: () =>
          import(/* webpackChunkName: "view-4" */ "../views/View4.vue"),
        meta: {
          private: true,
        },
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
  },
  {
    path: "*",
    name: "404",
    component: () => import(/* webpackChunkName: "404" */ "../views/404.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.meta.private) {
    sessionStorage.setItem("go", to.name);
    const isLoggedIn = sessionStorage.getItem("isLoggedIn");
    if (isLoggedIn && JSON.parse(isLoggedIn)) return next();
    return next({ name: "login" });
  } else {
    next();
  }
});

export default router;
