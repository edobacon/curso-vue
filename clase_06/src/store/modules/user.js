import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default {
  namespaced: true,
  state: {
    userName: "",
  },
  mutations: {
    setUserName(state, payload) {
      state.userName = payload;
    },
  },
  getters: {
    userName(state) {
      return state.userName;
    },
  },
};
