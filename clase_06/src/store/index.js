import Vue from "vue";
import Vuex from "vuex";
import user from "./modules/user";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
  },
  state: {
    count: 0,
  },
  mutations: {
    increment(state) {
      state.count++;
    },
    decrement(state) {
      state.count--;
    },
    changeValue(state, payload) {
      state.count += payload;
    },
  },
  actions: {
    changeValueAction({ commit }, ammount) {
      // async code
      commit("changeValue", ammount);
    },
  },
  getters: {
    countValue: (state) => state.count,
  },
});
