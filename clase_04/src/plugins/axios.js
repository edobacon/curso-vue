import axios from "axios";

// axios.defaults.baseURL = "https://jsonplaceholder.typicode.com";

axios.interceptors.request.use(
  (config) => {
    console.log("interceptando un request", config);
    return config;
  },
  (error) => {
    console.log("error en request", error);
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => {
    console.log("interceptando un response", response);
    return response;
  },
  (error) => {
    console.log("error en request", error);
    return Promise.reject(error);
  }
);
